package ckafka

import (
	"fmt"

	"duynd2002/gokits/libs/ilog"
	"github.com/IBM/sarama"
)

func (c *ClientKafka) ProducerPushMessage(topic string, message string) (partition int32, offset int64, err error) {
	if c == nil || c.producer == nil {
		return 0, 0, fmt.Errorf("ClientKafka::ProducerPushMessage - Not found any producer")
	}

	ilog.Infof("ClientKafka::ProducerPushMessage - Push to topic: %s object data: %s", topic, message)

	msg := &sarama.ProducerMessage{
		Topic: topic,
		Value: sarama.StringEncoder(message),
	}

	return c.producer.SendMessage(msg)
}

func (c *ClientKafka) ProducerPushMessageWithKey(topic, key string, message string) (partition int32, offset int64, err error) {
	if c == nil || c.producer == nil {
		return 0, 0, fmt.Errorf("ClientKafka::ProducerPushMessage - Not found any producer")
	}

	ilog.Infof("ClientKafka::ProducerPushMessage - Push to topic: %s, key: %s, object data: %s", topic, key, message)

	msg := &sarama.ProducerMessage{
		Topic: topic,
		Key:   sarama.StringEncoder(key),
		Value: sarama.StringEncoder(message),
	}

	return c.producer.SendMessage(msg)
}

func (c *ClientKafka) ProducerPushMessagesTxt(topic string, messages []string) (err error) {
	if c == nil || c.producer == nil {
		return fmt.Errorf("ClientKafka::ProducerPushMessage - Not found any producer")
	}
	if err := c.producer.BeginTxn(); err != nil {
		ilog.Errorf("could not begin transaction: %v", err)
		return fmt.Errorf("ClientKafka::ProducerPushMessage - could not begin transaction")
	}
	msges := make([]*sarama.ProducerMessage, 0)
	for _, message := range messages {
		msg := &sarama.ProducerMessage{
			Topic: topic,
			Value: sarama.StringEncoder(message),
		}
		msges = append(msges, msg)
	}

	err = c.producer.SendMessages(msges)
	if err != nil {
		if err = c.producer.AbortTxn(); err != nil {
			ilog.Errorf("could not abort transaction: %v", err)
			return fmt.Errorf("ClientKafka::ProducerPushMessage - could not abort transaction")
		}
	}

	if err := c.producer.CommitTxn(); err != nil {
		ilog.Errorf("could not commit transaction: %v\n", err)
		if err = c.producer.AbortTxn(); err != nil {
			ilog.Errorf("could not abort transaction: %v", err)
			return fmt.Errorf("ClientKafka::ProducerPushMessage - could not abort transaction")
		}
	}
	return nil
}
