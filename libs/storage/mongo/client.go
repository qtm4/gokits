package mongo_client

import (
	"context"
	"fmt"

	"duynd2002/gokits/libs/ilog"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func ConnectMongoDB() *mongo.Client {
	ilog.Infof("ConnectMongoDB processing...")
	LoadMongoConfig()
	uri := ""
	if configs.MongoURI != "" {
		uri = configs.MongoURI
	} else {
		uri = fmt.Sprintf("mongodb://%v:%v@%v:%v/?authSource=%v&%v", configs.UserName, configs.Password, configs.Host, configs.Port, configs.AuthSource, configs.Extra)
	}
	clientOptions := options.Client().ApplyURI(uri)
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		ilog.Errorf("Error connecting to Mongo: %v\n", err)
	}
	return client
}
