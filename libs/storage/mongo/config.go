package mongo_client

import (
	"time"

	"duynd2002/gokits/libs/ilog"
	"github.com/spf13/viper"
)

const (
	KDefaultTimeout = 30 * time.Second
)

type NoSQLConfig struct {
	MongoURI   string
	UserName   string
	Password   string
	Host       string
	Port       int
	Extra      string
	AuthSource string
}

var configs *NoSQLConfig

// default value env key is "MySQL";
// if configKeys was set, key env will be first value (not empty) of this;
func LoadMongoConfig() {
	if err := viper.UnmarshalKey("Mongo", &configs); err != nil {
		ilog.Errorf("getGDiscoveryServerConfigFromEnv - Error: %v", err)
		panic(err)
	}
	if configs.MongoURI == "" && (configs.Host == "" || configs.Port == 0) {
		panic("load mongo client config empty")
	}
}
