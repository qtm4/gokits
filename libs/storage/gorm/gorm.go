package gorm

import (
	"context"
	"database/sql"

	"duynd2002/gokits/libs/ilog"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type GormClient struct {
	Db    *gorm.DB
	SqlDB *sql.DB
}

// NewRPCClient func
func NewGormClient(conf *GormConfig) *GormClient {
	db, err := gorm.Open(mysql.Open(conf.DSN), &gorm.Config{})
	if err != nil {
		panic(err)
	}

	sqlDB, err := db.DB()
	if err != nil {
		panic(err)
	}

	// Thiết lập số lượng kết nối trong pool
	sqlDB.SetMaxIdleConns(conf.Idle)
	sqlDB.SetMaxOpenConns(conf.Active)

	ilog.Infof("[-] Installed sqlgorm client done!")

	return &GormClient{
		Db:    db,
		SqlDB: sqlDB,
	}
}

type txContextKey struct{}

func (d *GormClient) TransactionGorm(ctx context.Context, fn func(ctx context.Context) error) (err error) {
	tx := d.Db.Begin()
	if err != nil {
		return err
	}
	var done bool

	defer func() {
		if !done {
			_ = tx.Rollback()
		}
	}()
	if err = fn(context.WithValue(ctx, txContextKey{}, tx)); err != nil {
		return err
	}
	done = true
	return tx.Commit().Error
}
