package cmongo

import (
	"fmt"
	"sync"

	"duynd2002/gokits/libs/ilog"
)

type mongoClientManager struct {
	mongoClients sync.Map
}

var mongoClientsManagerInstance = &mongoClientManager{}

func InstallMongoClientManager(configKeys ...string) {
	getConfigFromEnv(configKeys...)

	for _, config := range configs {
		client := NewMongoDB(config)
		if client == nil {
			err := fmt.Errorf("InstallMongoClientManager - NewSqlxDB {%v} error", config)
			ilog.Errorf("InstallMongoClientManager - Error: %v", err)

			panic(err)
		}

		if config.Name == "" {
			err := fmt.Errorf("InstallMongoClientManager - config error: config.Name is empty")
			ilog.Errorf("InstallMongoClientManager - Error: %v", err)

			panic(err)
		}
		if val, ok := mongoClientsManagerInstance.mongoClients.Load(config.Name); ok {
			err := fmt.Errorf("InstallMongoClientManager - config error: duplicated config.Name {%v}", val)
			ilog.Errorf("InstallMongoClientManager - Error: %v", err)

			panic(err)
		}

		mongoClientsManagerInstance.mongoClients.Store(config.Name, client)
	}
}

func GetMongoClient(dbName string) (client *MongoClient) {
	if val, ok := mongoClientsManagerInstance.mongoClients.Load(dbName); ok {
		if client, ok = val.(*MongoClient); ok {
			return client
		}
	}

	ilog.Infof("GetMongoClient - Not found client: %s", dbName)
	return
}
