package cmongo

import (
	"context"

	"duynd2002/gokits/libs/ilog"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoClient struct {
	*mongo.Database
}

func NewMongoDB(c *MongoConfig) *MongoClient {
	ctx, cancel := context.WithTimeout(context.Background(), KDefaultTimeout)
	defer cancel()

	clientOptions := options.Client().ApplyURI(c.URI)
	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		ilog.Errorf("NewMongoDB mongo.Connect error: %+v", err)
	}
	err = client.Ping(ctx, nil)
	if err != nil {
		ilog.Errorf("NewMongoDB client.Ping error: %+v", err)
	}
	db := client.Database(c.Name)
	ilog.Infof("[=]NewMongoDB: [dbname]: %s")

	return &MongoClient{db}
}
