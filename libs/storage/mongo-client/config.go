package cmongo

import (
	"fmt"
	"strings"
	"time"

	"github.com/spf13/viper"
)

var (
	Name string
	URI  string
)

const (
	KDefaultTimeout = 30 * time.Second

	KDB_MYPOINT = "mypoint"
)

type MongoConfig struct {
	Name string `json:"name,omitempty"`
	URI  string `json:"uri,omitempty"`
}

var configs []*MongoConfig

// default value env key is "MySQL";
// if configKeys was set, key env will be first value (not empty) of this;
func getConfigFromEnv(configKeys ...string) {
	configKey := "Mongo"
	for _, envKey := range configKeys {
		envKeyTrim := strings.TrimSpace(envKey)
		if envKeyTrim != "" {
			configKey = envKeyTrim
		}
	}

	raw := make([]*MongoConfig, 0)

	if err := viper.UnmarshalKey(configKey, &raw); err != nil {
		err := fmt.Errorf("not found config name with env %q for Mongo with error: %+v", configKey, err)
		panic(err)
	}

	configs = make([]*MongoConfig, 0)
	for _, config := range raw {
		if config.URI == "" {
			continue
		}

		if config.Name == "" {
			config.Name = "immaster"
		}

		configs = append(configs, config)
	}

	if len(configs) == 0 {
		err := fmt.Errorf("not found valid config with env %q for Mongo", configKey)
		panic(err)
	}
}
