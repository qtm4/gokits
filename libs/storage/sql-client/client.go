package csql

import (
	"time"

	_ "github.com/go-sql-driver/mysql" // import your used driver
	"github.com/jmoiron/sqlx"

	"duynd2002/gokits/libs/ilog"
)

type ISqlClient interface {
	sqlx.ExtContext
}

type SQLClient struct {
	*sqlx.DB
}

func (client *SQLClient) Get() *sqlx.DB {
	return client.DB
}

// NewSqlxDB type;
// For MySQL, posgreSQL
func NewSqlxDB(c *SQLConfig) *SQLClient {
	db, err := sqlx.Connect(c.Driver, c.DSN)
	if err != nil {
		ilog.Errorf("NewSqlxDB Connect db error: %+v", err)
	}

	db.SetMaxOpenConns(c.Active)
	db.SetMaxIdleConns(c.Idle)
	if c.Lifetime < 60 {
		c.Lifetime = 5 * 60
	}
	db.SetConnMaxLifetime(time.Duration(c.Lifetime) * time.Second)

	ilog.Infof("[=]NewSqlxDB: [dbname]: %s, [setting]: %+v", c.Name, db.Stats())

	return &SQLClient{db}
}
