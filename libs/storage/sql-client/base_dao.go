package csql

import (
	"context"
	"database/sql"
	"sync"

	"duynd2002/gokits/libs/storage"
	"github.com/jmoiron/sqlx"
)

var mapBaseDao = sync.Map{}
var mapBaseDB = sync.Map{}

type BaseMysqlDAO struct {
	Client ISqlClient
}

type BaseMysqlDB struct {
	Client *sql.DB
}

func NewBaseMysqlDAO(dbName string) *BaseMysqlDAO {
	return storage.LoadDAO(
		dbName,
		&mapBaseDao,
		GetSQLClientManager(),
		func(client interface{}) *BaseMysqlDAO {
			return &BaseMysqlDAO{Client: client.(*SQLClient)}
		},
	)
}

func NewBaseMysqlDB(dbName string) *SQLClient {
	return storage.LoadDAO(
		dbName,
		&mapBaseDB,
		GetSQLClientManager(),
		func(client interface{}) *SQLClient {
			return client.(*SQLClient)
		},
	)
}

type txContextKey struct{}

func (d *BaseMysqlDAO) Transaction(ctx context.Context, fn func(ctx context.Context) error) (err error) {
	tx, err := d.Client.(*SQLClient).BeginTxx(ctx, &sql.TxOptions{})
	if err != nil {
		return err
	}
	var done bool

	defer func() {
		if !done {
			_ = tx.Rollback()
		}
	}()
	if err = fn(context.WithValue(ctx, txContextKey{}, tx)); err != nil {
		return err
	}
	done = true
	return tx.Commit()
}

func (d *BaseMysqlDAO) TransactionWithOption(ctx context.Context, option *sql.TxOptions, fn func(ctx context.Context) error) (err error) {
	tx, err := d.Client.(*SQLClient).BeginTxx(ctx, option)
	if err != nil {
		return err
	}
	var done bool

	defer func() {
		if !done {
			_ = tx.Rollback()
		}
	}()
	if err = fn(context.WithValue(ctx, txContextKey{}, tx)); err != nil {
		return err
	}
	done = true
	return tx.Commit()
}

func (d *BaseMysqlDAO) GetDB(ctx context.Context) ISqlClient {
	value := ctx.Value(txContextKey{})
	if value != nil {
		return value.(*sqlx.Tx)
	}
	return d.Client
}
